#include <stdio.h>
#include <string.h>
#include <ctype.h>


void sort_dictionary(char dictionary[1000][201], int count[1000], int index)
{
	int num=index;
	int temp;
	char phrase[201];

	for (int i=0;i<index;i++)//bubble sort that sorts the dictionary by phrase length
	{
		for (int j=0;j<num;j++)
		{
			if (strlen(dictionary[j])>strlen(dictionary[j+1]))//if phrase j is longer than the next phrase, swap the two phrases
			{
				temp=count[j+1];//save count of second phrase
				count[j+1]=count[j];//swap two counts
				count[j]=temp;
		
				strcpy(phrase,dictionary[j+1]);//save second phrase
				strcpy(dictionary[j+1],dictionary[j]);//swap two phrases
				strcpy(dictionary[j],phrase);
			}	
		}
	}
	return;
}	
