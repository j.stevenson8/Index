#include <stdio.h>
#include <string.h>
#include <ctype.h>

void phrase_capture(char [120]);
int isPunct(char);
char get_char();
char phrase_init();

void phrase_capture(char phrase[201])
{
	char temp=phrase_init();//finds the initial character of the next phrase
	int i=1;
	phrase[0]=temp;//records initial character to first element of the temporary phrase
	temp=get_char();//get a character using get_char
	while ((isPunct(temp)==0)&&(i<199))//as long the phrase is not terminated and it is not over 200 characters
	{
		phrase[i++]=temp;//records next element of phrase
		if (i<199)
		{
			temp=get_char();//get another character as long as the phrase is not over 200 characters long
		}
	}

	phrase[i]='\0';//sets last element of the phrase to the null terminator
	return;
}		
