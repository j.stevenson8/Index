#include <stdio.h>
#include <string.h>
#include <ctype.h>



int phrase_compare(char input[201],char dictionary[1000][201],int count[1000],int index)
{
	int isSame=1;
	int i=-1;
		
	while (i<(index+1))//as long as i is less than or equal to the index of dictionary
	{
		isSame=strcmp(input,dictionary[++i]);//compare the input to a phrase in dictionary
		if (isSame==0)//if they are the same, increment the count of that phrase 
		{
			count[i]++;
			return index;
		}
	}
	if (index<999)//as long as there are less than 1000 phrases, add the phrase to the dictionary
	{
		strcpy(dictionary[++index],input);
		count[i]=1;//sets count to one 
	}
	
	return index;//returns new index of dictionary
}
