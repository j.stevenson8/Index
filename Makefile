COMPILEFLAGS=-g
CCOMP=gcc

prog: main.o phrase_capture.o phrase_init.o phrase_compare.o sort_dictionary.o get_char.o print_dictionary.o
	$(CCOMP) $(COMPILEFLAGS) -o prog main.o phrase_capture.o phrase_init.o phrase_compare.o sort_dictionary.o get_char.o print_dictionary.o
 
main.o: main.c phrase_capture.c phrase_compare.c sort_dictionary.c print_dictionary.c
	$(CCOMP) $(COMPILEFLAGS) -c main.c phrase_capture.c phrase_compare.c sort_dictionary.c print_dictionary.c
 
phrase_capture.o: phrase_init.c get_char.c
	$(CCOMP) $(COMPILEFLAGS) -c phrase_init.c get_char.c
 
phrase_init.o: get_char.c phrase_init.c
	$(CCOMP) $(COMPILEFLAGS) -c get_char.c phrase_init.c
 
get_char.o: get_char.c 
	$(CCOMP) $(COMPILEFLAGS) -c get_char.c
 
phrase_compare.o: phrase_compare.c
	$(CCOMP) $(COMPILEFLAGS) -c phrase_compare.c
 
sort_dictionary.o: sort_dictionary.c
	$(CCOMP) $(COMPILEFLAGS) -c phrase_compare.c
 
print_dictionary.o: print_dictionary.c
	$(CCOMP) $(COMPILEFLAGS) -c print_dictionary.c

clean: 
	rm main.o phrase_capture.o phrase_init.o phrase_compare.o sort_dictionary.o get_char.o print_dictionary.o 
