#include <stdio.h>
#include <string.h>
#include <ctype.h>

char phrase_init();
int isPunct(char);
char get_char();

char phrase_init()
{
	char temp=get_char();//get a character

	while ((isPunct(temp)==1)||(temp==' '))//if it's punctuation, get another one
	{
		temp=get_char();
	}
	return temp;
}

int isPunct(char temp)//detects punctuation
{
	if ((temp==',')||(temp=='.')||(temp=='?')||(temp==';')||(temp==':')||(temp=='!'))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
