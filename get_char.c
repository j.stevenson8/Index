#include <stdio.h>
#include <string.h>
#include <ctype.h>

char get_char();
int isIgnore(char);

extern int isEOF;

char get_char()
{
	int temp=getchar(); //gets initial character

	while (isIgnore(temp)==1) //keeps looking for a character that isn't ignored
	{
		temp=getchar();
	}

	temp=toupper(temp); //goes to uppercase
	
	if (isspace(temp)!=0)//turns certain characters to a space
	{
		temp=' ';
	}

	if (temp==EOF)//detects end of file 
	{
		isEOF=1;//sets EOF flag to one
		temp='\0';//sets the temporary character to the null terminator to ensure no "funny business"
	}
	return temp;
}

int isIgnore(char temp)//detects ignored characters
{
	if ((temp=='(')||(temp==')')||(temp=='-')||(temp=='"')||(temp=='\''))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

