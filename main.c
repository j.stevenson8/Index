/*
John Stevenson
CSE 224
PA4
This program counts the frequency of unique phrases from standard input and prints the result to the screen
*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

char get_char();
int isIgnore(char);
char phrase_init();
int isPunct(char);
void phrase_capture(char [201]);
int phrase_compare(char [201],char[1000][201],int[1000],int);
void print_dictionary(char[1000][201],int[1000],int);
void sort_dictionary(char[1000][201],int[1000],int);

int isEOF=0;

void main()
{
	char phrase[201];//char array used to store a temporary phrase
	int count[1000];//int array storing the number of times each phrase was recorded
	char dictionary[1000][201];//an array of strings containing the recorded phrases
	int index=-1;//index of dictionary
		
	while (isEOF==0)
	{
		phrase_capture(phrase);//captures a phrase from stdin
		index=phrase_compare(phrase,dictionary,count,index);//compares the phrase with dictionary and either increments the count of an existing phrase or creates a new one	
	}
	sort_dictionary(dictionary,count,index);//sorts the dictionary by phrase length
	print_dictionary(dictionary,count,index);//prints the dictionary and the count of each phrase
	return;
}

